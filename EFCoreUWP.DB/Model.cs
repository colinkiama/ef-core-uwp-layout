﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using Microsoft.EntityFrameworkCore;
using Windows;
using Windows.Storage;
using Windows.Foundation;
using Microsoft.EntityFrameworkCore.Design;

namespace EFCoreUWP.DB
{
    public class BloggingContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                options.UseSqlite("Data Source=blogging.db");
            }

        }
        public BloggingContext() : base() { }

        public BloggingContext(DbContextOptions<BloggingContext> options)
        : base(options)
        { }

        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

        public List<Post> Posts { get; private set; } = new List<Post>();
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public Blog Blog { get; set; }
    }
}