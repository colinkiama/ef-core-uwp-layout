﻿using EFCoreUWP.DB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace EFCoreUWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void StartDBButton_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new BloggingContext(DatabaseOptionsBuilders.BloggingContextOptionBuilder.Options))
            {
                // Create
                Debug.WriteLine("Inserting a new blog");
                db.Add(new Blog { Url = "http://blogs.msdn.com/adonet" });
                await db.SaveChangesAsync();

                // Read
                Debug.WriteLine("Querying for a blog");
                var blogList = db.Blogs
                    .OrderBy(b => b.BlogId);

                foreach (var blog in blogList)
                {
                    Debug.WriteLine($"Blog: {blog.BlogId}, {blog.Url}");
                }

                var firstBlog = blogList.First();
                // Update
                Debug.WriteLine("Updating the blog and adding a post");
                firstBlog.Url = "https://devblogs.microsoft.com/dotnet";
                var posts = firstBlog.Posts;
                firstBlog.Posts.Add(
                    new Post
                    {
                        Title = "Hello World",
                        Content = "I wrote an app using EF Core!",
                    });
                await db.SaveChangesAsync();

                //Delete
                Debug.WriteLine("Delete the blog");
                db.Remove(firstBlog);
                await db.SaveChangesAsync();
            }
        }
    }
}
