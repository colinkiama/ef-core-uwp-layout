﻿using EFCoreUWP.DB;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Windows.Storage;

namespace EFCoreUWP
{
    public static class DatabaseOptionsBuilders
    {
        public static DbContextOptionsBuilder<BloggingContext> BloggingContextOptionBuilder { get; }
        static DatabaseOptionsBuilders()
        {
            var dbPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "blogging.db");
            BloggingContextOptionBuilder = new DbContextOptionsBuilder<BloggingContext>();
            BloggingContextOptionBuilder.UseSqlite($"Data Source={dbPath}");
        }
         
    }
}
